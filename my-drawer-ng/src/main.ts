import { platformNativeScript, runNativeScriptAngularApp } from '@nativescript/angular';

import { registerElement } from "nativescript-angular/element-registry";
import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { AppModule } from "./app/app.module";

registerElement("PullToRefresh", () => require("nativescript-pulltorefresh").PullToRefresh);
 platformNativeScriptDynamic().bootstrapModule(AppModule);

runNativeScriptAngularApp({
  appModuleBootstrap: () => platformNativeScript().bootstrapModule(AppModule),
});

