import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core'
import { NativeScriptModule } from '@nativescript/angular'
import { NativeScriptUISideDrawerModule } from 'nativescript-ui-sidedrawer/angular'

import { EffectsModule } from "@ngrx/effects";
import { ActionReducerMap, StoreModule as NgRxStoreModule } from "@ngrx/store";
import { AppRoutingModule } from './app-routing.module'
import { AppComponent } from './app.component';
import { DomainComponent } from './domain/domain.component';
import { ListadoComponent } from './listado/listado.component';
import { VistaDetalleComponent } from './vista-detalle/vista-detalle.component'
import { 
  initializeNoticiasState,
  NoticiasEffects,
  NoticiasState,
  reducersNoticias
} from "./domain/noticias-state.model";
import { NoticiasService } from './domain/noticias.service'

// redux init
// tslint:disable-next-line:interface-name
export interface AppStore {
  noticias: NoticiasState;
}

const reducers: ActionReducerMap<AppState> = {
  noticias: reducersNoticias
};

const reducersInitialState = {
  noticias: initializeNoticiasState()
};

// fin redux init

@NgModule({
  bootstrap: [AppComponent],
  imports: [AppRoutingModule, NativeScriptModule, NativeScriptUISideDrawerModule, NgRxStoreModule.forRoot(reducers, {initialState: reducersInitialState}), EffectsModule.forRoot([NoticiasEffects])],
  providers: [NoticiasService],
  declarations: [AppComponent, DomainComponent, ListadoComponent, VistaDetalleComponent],
  schemas: [NO_ERRORS_SCHEMA],
})
export class AppModule {}
