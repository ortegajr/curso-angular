import { Component, EventEmitter, NgModule, Output } from '@angular/core';
import { Button, TextField } from '@nativescript/core';

@Component({
  selector: 'SearchForm',
  moduleId: module.id,
  template: '<TextField [(NgModel)]="textFieldValue" hint="Enter text..."></TextField><Button text="Button" (tap)="onButtonTap()"></Button>'
  
})
export class SearchFormComponent {
  textFieldValue: string = "";
  @Output() search: EventEmitter<string> = new EventEmitter();
  @Input() inicial: string;

ngOnInit(): void{
  this.textFieldValue = this.inicial;
}

  onButtonTap(): void{
    console.log(this.textFieldValue);
    if (this.textFieldValue.length > 2) {
      this.search.emit(this.textFieldValue);
    }
  }

}
