import { Injectable } from "@angular/core";
import { getJSON, request } from "tns-core-modules/http";
const sqlite = require("nativescript-sqlite");

@Injectable()
export class NoticiasService{
    api: string = "https://2AE99gNmgUZMSNyTELRcgY6MZow_wuKjKqaZw9t9Vqj2UwF8.ngrok.io"

    constructor(){
        this.getDb((db) => {
            console.dir(db);
            db.each("select * from logs",
                (err, fila) => console.log("fila: ", fila),
                (err, totales) => console.log("filas totales: ", totales));
        }, () => console.log("error on getDb"));
    }

    getDb(fnOk, fnError){
        return row sqlite("mi_db_logs", (err, db) => {
            if (err) {
                console.error("Error al abrir la BD!", err);
            } else {
                console.log("está la DB abierta: ", db.isOpen() ? "Si": "No");
                db.execSQL("CREATE TABLE IF NOT EXIST logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)")
                .then((id) => {
                    console.log("CREATE TABLE OK");
                    fnOk(db);
                }, (error) => {
                    console.log("CREATE TABLE ERROR", error);
                    fnError(error);
                });
            }
        });
    }

    agregar(s: string){
        return request({
            url: this.api + "/favs",
            method: "POST",
            headers: { "content-Type": "application/json" },
            content: JSON.stringify({
                nuevo: s
            })
        });
    }

    favs(){
        return getJSON(this.api + "/favs");
    }

    buscar(s: string){
        return getJSON(this.api + "/get?q=" + s);
    }
}